package exam;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Unit test for simple App.
 */
@RunWith(Parameterized.class)
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    exam1 exam;
    private int n;
    boolean exp;

    public AppTest(int n, Boolean exp){
        this.n = n;
        this.exp = exp;
    }


    @Before
    public void setup(){
        exam = new exam1();
    }

    @Parameters
    public static Collection<Object[]> nums(){
        return Arrays.asList(new Object[][]{
            {0, false},
            {4, false},
            {2, true}
        });
    }

    @Test
    public void TestExam1()
    {
        assertEquals(exp, exam.exam(n));
    }
}
